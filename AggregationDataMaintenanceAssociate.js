// Christopher W. Carter
// Ricroar.Cypher@gmail.com
// https://bitbucket.org/RicroarCypher
//
// Indeed.com - AggregationDataMaintenanceAssociate.js
//   ==> http://pastebin.com/FkvsVQBv
// 
// An Example of the indeed-AggregationDataMaintenanceAssociate.js library in use
// Indeed.com - AggregationDataMaintenanceAssociate.html
//   ==> http://pastebin.com/R4EBPPgr

function getElementByContents(contents, tag) {
    /// Returns undefined if contents cannot be found
    "use strict";
    var arrayTags = document.getElementsByTagName(tag);
    var i;
    for (i = 0; i < arrayTags.length; i++) {
        if (arrayTags[i].innerHTML === contents) {
            return arrayTags[i];
        }
    }
}
// Example of usage:
//     Job Title:
//     <input id="searchbox" type="text" value="Aggregation Data Maintenance Associate">
//     <button onclick="var x=getJobTitleGeolocation(searchbox.value); this.innerHTML = x">Try it</button>
function getJobTitleGeolocation(jobTitle) {
    /// Returns undefined if Job Title cannot be found

    "use strict";
    // Get element of a job by its title - easily access innerHTML and href
    var foundJob = getElementByContents(jobTitle, "a");
    if (foundJob === 'undefined') {
        // Job title was not found
        return foundJob;
    }

    // Setup CSS Selector for specific Job Element by Job's URL
    var jobDesired = "ul>li>a[href='" + foundJob.getAttribute("href") + "']";

    var geolocation = document.querySelectorAll(".job-cat");
    var searchForMe;
    var i;
    for (i = 0; i < geolocation.length; i++) {
        // Assign an id attribute to make CSS elector easier
        var city = geolocation[i].innerHTML.replace(", ", "").replace(" ", "", "g");
        geolocation[i].setAttribute("id", city);

        // Create CSS selector
        searchForMe = "#" + geolocation[i].getAttribute("id") + "+" + jobDesired;

        // Find and return matching Geolocation Info
        if (document.querySelector(searchForMe) !== null) {
            return geolocation[i].innerHTML;
        }
    }
    // Geolocation should have been found by this point
}
