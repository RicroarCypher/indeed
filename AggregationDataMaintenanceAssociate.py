#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Christopher W. Carter
# Ricroar.Cypher@gmail.com
# https://bitbucket.org/RicroarCypher
#
# Indeed.com - AggregationDataMaintenanceAssociate.py
# http://pastebin.com/FkvsVQBv

from urllib.request import urlopen
from urllib.error import HTTPError
from html.parser import HTMLParser


class HTMLParser_besmith(HTMLParser):
	"""https://www.besmith.com/candidates/search-listings"""
    def __init__(self):
        HTMLParser.__init__(self)
        self.inRow = False
        self.inColumn = False
        self.inLink = False
        self.found_char_ref = False
        self.list_of_jobs = []
        self.job_data = []
        self.base_url = "https://www.besmith.com"

    def handle_starttag(self, tag, attrs):
        if tag == 'tr':
            self.inRow = True
            self.job_data.clear()
        if tag == 'td' and self.inRow:
            self.inColumn = True
        if tag == 'a' and self.inRow and self.inColumn:
            self.inLink = True
            for key, value in attrs:
                if key == 'href':
                    # https://www.besmith.com + /candidates/...
                    self.job_data.append(self.base_url + value)

    def handle_endtag(self, tag):
        if tag == 'tr':
            self.inRow = False
            self.list_of_jobs.append(self.job_data.copy())
        if tag == 'td' and self.inRow:
            self.inColumn = False
            # prevent apostrophe from stepping on other column data
            self.found_char_ref = False
        if tag == 'a' and self.inRow and self.inColumn:
            self.inLink = False

    def handle_charref(self, name):
        if self.inRow and self.inColumn:
            if name.startswith('x'):
                c = chr(int(name[1:], 16))
            else:
                c = chr(int(name))
            self.found_char_ref = True
            # An apostrophe (&#39;) showed up
            #   Merge it with prevous element
            self.job_data[-1] = self.job_data[-1] + c

    def handle_data(self, data):
        if self.inRow and self.inColumn:
            if data.rstrip(' ') != '':
                if self.found_char_ref:
                    # An apostrophe was found,
                    #   Merge content data with prevous element
                    self.found_char_ref = False
                    self.job_data[-1] = self.job_data[-1] + data
                else:
                    self.job_data.append(data)

# get/manlipulate url if necessary
#   Page 2 of https://www.besmith.com/candidates/search-listings
url = "https://www.besmith.com/candidates/search?field_state_value=All&field_job_category_tid=&field_job_type_tid=&field_code_value=&body_value=&field_term_tags_value=&page=1&title="

# download webpage
try:
    page = urlopen(url)
except HTTPError as err:
    print("HTTPError:", errors.messages[err.code])

# parse webpage based upon custom HTML parser for Besmith.com
parser = HTMLParser_besmith()
parser.feed(page.read().decode('utf-8'))

# CSV => ','
delimiter = ','
# Easier to know what is CSV vs data e.g. City, State.
#   Also easy to remove if undesired -> quoter = ''
quoter = '"'
# Remove Table Head Row
parser.list_of_jobs.pop(0)
for jobdata in parser.list_of_jobs:
    # Could convert to dictionaries if there was further need to manlipulate
    # jobdata[0] => Job URL
    # jobdata[1] => Job Title
    # jobdata[2] => Organization Name
    # jobdata[3] => Position Type
    # jobdata[4] => Location
    data = [jobdata[1], jobdata[0], jobdata[4]]
    # Insert commas for CSV and quotation marks around data for element of the
    #   row then remove last element of the list (a delimiter) and join into
    #   a string
    data = ''.join(sum([[quoter, i, quoter, delimiter] for i in data.copy()], [])[:-1])
    print(data)
